package com.stmn.calculaimc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class Sobre extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        //separador
        String s = "\n\n";

        //Notas da versão
        String n1 = "v0.1 - Aplicativo criado, criadas as funções de cálculo do IMC. Foram criados alertas específicos para cada nível de IMC.";
        String n2 = "v0.5 - Correções de bugs, falhas no cálculo corrigidas. Melhorias no sistema de alertas.";
        String n3 = "v0.9 - Tratamento de erros, cálculo agora pode ser feito tanto com a medida em metros quanto em centímetros, melhorias na interface";
        String n4 = "v1.0 - Primeira versão estável lançada, criado o sistema de notas da versão e a versão atual é informada na tela principal.";

        //TextView que exibe as notas da versãp na tela, aqui o texto é modificado a cada nota nova
        TextView notas = (TextView) findViewById(R.id.notasDaVersao);

        notas.setText("\n\n" + n4 + s + n3 + s + n2 + s + n1);
    }

    public void sobreDesenvolvedor(View view) {
        Intent intent = new Intent(this, com.stmn.calculaimc.SobreDesenvolvedor.class);
        startActivity(intent);
    }
}