package com.stmn.calculaimc;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.NumberFormat;
import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Cria a View de propaganda
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        Double versaoCod = 1.0;
        String versaoName = "BETA";

    }
    public void calcular (View view) {

        Locale.setDefault(Locale.US);

        EditText et_altura = (EditText) findViewById(R.id.edit_altura);
        EditText et_peso = (EditText) findViewById(R.id.edit_peso);
        TextView textView = (TextView) findViewById(R.id.textView);
        TextView textView2 = (TextView) findViewById(R.id.textView2);

        //condição para dar o resultado
        if (et_altura.getText().length() != 0
                && et_peso.getText().length() != 0
                && Float.parseFloat(et_peso.getText().toString()) < 300
                && Float.parseFloat(et_altura.getText().toString()) < 300
                && Float.parseFloat(et_altura.getText().toString()) > 0.8 ) {

            double altura = Float.parseFloat(et_altura.getText().toString());
            double peso = Float.parseFloat(et_peso.getText().toString());
            double fatorAltura;
            double resultado;
            double pesoidealmin;
            double pesoidealmax;

            //define o Double para no máximo dois dígitos após a vírgula
            NumberFormat format = NumberFormat.getInstance();
            format.setMaximumFractionDigits(2);

            //calculo do IMC
            if (Double.valueOf(altura) < 3) {
                fatorAltura = altura*altura;
            }
            else {
                fatorAltura = altura / 100 * altura / 100;
            }
            resultado = peso / fatorAltura;
            resultado = Double.valueOf(format.format(resultado));

            //calculo do peso minimo e maximo
            pesoidealmin = Double.valueOf(format.format(18.5*fatorAltura));
            pesoidealmax = Double.valueOf(format.format(24.99*fatorAltura));

            //método que define as textViews e esvazia os campos editáveis
            textView.setText("Segue abaixo o seu índice de massa corporal");
            alert("Seu IMC é " + resultado);
            et_altura.setText("");
            et_peso.setText("");

            //muito abaixo do peso ideal
            if ((resultado) <= 17.00){
                textView2.setText("Você se encontra muito abaixo do peso ideal, o peso ideal para a sua altura é  entre " + pesoidealmin + " kg e " + pesoidealmax + " kg" + ". Procure um especialista para mais informações");
                textView2.setTextColor(Color.argb(255, 255, 50, 50));
            }

            //um pouco abaixo do peso ideal
            if ((resultado) > 17.00 && (resultado) <= 18.49){
                textView2.setText("Você se encontra um pouco abaixo do peso ideal, o peso ideal para a sua altura é  entre " + pesoidealmin +" kg e " + pesoidealmax + " kg" + ". Procure um especialista para mais informações");
                textView2.setTextColor(Color.argb(255, 255, 100, 100));
            }

            //no peso ideal
            if ((resultado) >18.5 && (resultado) <= 24.99){
                textView2.setText("Você se encontra no peso ideal, que para sua altura é entre " + pesoidealmin +" kg e " + pesoidealmax + " kg" + ". Procure um especialista para mais informações");
                textView2.setTextColor(Color.argb(255, 0, 255, 25));
            }

            //acima do peso
            if ((resultado) > 25.00 && (resultado) <= 29.99){
                textView2.setText("Você se encontra um pouco acima do peso ideal, o peso ideal para a sua altura é  entre " + pesoidealmin +" kg e " + pesoidealmax + " kg" + ". Procure um especialista para mais informações");
                textView2.setTextColor(Color.argb(255, 255, 100, 100));
            }

            //obesidade 1 (leve)
            if ((resultado) > 30.00 && (resultado) < 34.99){
                textView2.setText("Você está com obesidade 1 (leve), o peso ideal para a sua altura é  entre " + pesoidealmin +" kg e " + pesoidealmax + " kg" + ". Procure um especialista para mais informações");
                textView2.setTextColor(Color.argb(255, 255, 75, 75));
            }

            //obesidade 2 (severa)
            if ((resultado) > 35.00 && (resultado) < 39.99){
                textView2.setText("Você está com obesidade 2 (severa), o peso ideal para a sua altura é  entre " + pesoidealmin +" kg e " + pesoidealmax + " kg" + ". Procure um especialista para mais informações");
                textView2.setTextColor(Color.argb(255, 255, 50, 50));
            }

            //obesidade 3 (mórbida)
            if ((resultado) > 40.00){
                textView2.setText("Você está com obesidade 3 (mórbida), o peso ideal para a sua altura é  entre " + pesoidealmin +" kg e " + pesoidealmax + " kg" + ". Procure um especialista para mais informações");
                textView2.setTextColor(Color.argb(255, 255, 0, 0));
            }
        }

        //condição para calcular o peso ideal usando apenas o campo de altura
        else if (et_altura.getText().length() != 0
                && et_peso.getText().length() == 0
                && Float.parseFloat(et_altura.getText().toString()) < 300
                && Float.parseFloat(et_altura.getText().toString()) > 0.8 ) {

            double altura = Float.parseFloat(et_altura.getText().toString());
            double fatorAltura;
            double pesoidealmin;
            double pesoidealmax;

            NumberFormat format = NumberFormat.getInstance();
            format.setMaximumFractionDigits(2);

            if (Double.valueOf(altura) < 3) {
                fatorAltura = altura*altura;
            }
            else {
                fatorAltura = altura / 100 * altura / 100;
            }

            pesoidealmin = Double.valueOf(format.format(18.5*fatorAltura));
            pesoidealmax = Double.valueOf(format.format(24.99*fatorAltura));
            et_altura.setText("");
            et_peso.setText("");
            textView.setText("");
            alert("o peso ideal para a sua altura é  entre " + pesoidealmin +" kg e " + pesoidealmax + " kg");
        }

        //alerta caso esteja com preenchimento incorreto
        else {
            alert("Favor preencher corretamente os campos, caso não saiba o peso, basta preencher a altura e será informado o peso ideal");
        }
    }

    //Caso o usuário clique no botão LIMPAR irá zerar todos os campos com esse método
    public void recomecar (View view) {
        EditText et_altura = (EditText) findViewById(R.id.edit_altura);
        EditText et_peso = (EditText) findViewById(R.id.edit_peso);
        TextView textView = (TextView) findViewById(R.id.textView);
        TextView textView2 = (TextView) findViewById(R.id.textView2);

        et_altura.setText("");
        et_peso.setText("");
        textView.setText("Olá, seja bem vindo ao aplicativo de cálculo de IMC! Basta preencher os campos abaixo e tocar no botão calcular.");
        textView2.setText("");

        alert("Campos apagados com sucesso!");
    }

    //alert é Toast.makeText criado para receber os alertas da aplicação
    private void alert (String s) {
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }

    //Chama a segunda tela da aplicação, onde serão colocadas as notas da versão
    public void sobre(View view) {
        Intent intent = new Intent (this, com.stmn.calculaimc.Sobre.class);
        startActivity(intent);
    }
}